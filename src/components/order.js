import React from 'react';
import axios from 'axios';


class Order extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        product_list: [],
        user_id: ''
         };
      }

      componentDidMount(){
        axios.get('http://localhost:3001/customer_orders?id='+localStorage.getItem('user_id'))
        .then(response => {
            this.setState({ product_list: response.data});
          })
          .catch(function (error) {
            console.log(error);
          })
      }

      renderTableData() {
        return this.state.product_list.map((product_list, index) => {
          const { id,name,price,order_date,image} = product_list
           return (
              <tr key={id}>
                  <td>{name}</td>
                 <td>{price}</td>
                 <td><img src={`http://localhost:3001/${image}`} style={{height:"140px",width:"500"}} alt="images"></img></td>
                 <td>{order_date}</td>
                </tr>
           )
        })
     }

     home=()=>{
      this.props.history.push({
        pathname:"/customer_shop",
        state:{user_id: this.state.user_id}
      })
  }


    render(){
        return(
            <div className="container">
            <h3 style={{textAlign:"center"}}> Your Orders</h3>
            <button className="btn btn-danger" onClick={this.home}>Home</button>
            <table className="table table-striped" style={{ marginTop: 20 }} >
            <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
              {this.renderTableData()}
        </tbody>
     </table>   
</div>
        );
    }
}


export default Order;