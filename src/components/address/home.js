import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {  NavLink } from "react-router-dom";

class Home extends React.Component{
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
        this.state = {
        product_list: [],
        user_id:''
        };
      }

      componentDidMount(){
        axios.get('http://localhost:3001/address_all?id='+this.props.location.state.user_id)
        .then(response => {
            this.setState({ product_list: response.data,user_id: this.props.location.state.user_id });
          })
          .catch(function (error) {
            console.log(error);
          })
      }

      logout() {
        localStorage.clear();
        window.location.href = '/';
      }

      delete() {
        axios.post('http://localhost:3001/product_delete/?id='+this.state.product_list[0].id)
        this.props.history.push({
          pathname:"/home",
          state:{user_id: this.state.user_id}})
      }

        renderTableData() {
            return this.state.product_list.map((product_list, index) => {
              const { id,name,price,stock,category} = product_list 
              
               return (
                  <tr key={id}>
                      <td>{name}</td>
                     <td>{price}</td>
                     <td>{stock}</td>
                     <td>{category.name}</td>
                     <td><NavLink to={"/edit/"+id} className="btn btn-success">Edit</NavLink></td>
                     <td><button to={"/delete/"+id} className="btn btn-primary" onClick={this.delete}>Delete</button></td>
                    </tr>
               )
            })
         }
         page=()=>{
             this.props.history.push({
               pathname:"/create",
               state:{user_id: this.state.user_id}
             })
         }

    render(){
        return(
            <div className="container">
                <h3 style={{textAlign:"center"}}>My Productlist</h3><button className="btn btn-info" onClick={this.page}>Create</button>
                <button className="btn btn-danger" style={{float:"right"}} onClick={this.logout}>Logout</button>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                <thead>
              <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Stock</th>
                <th>Category Name</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
                  {this.renderTableData()}
            </tbody>
         </table>   
   </div>
        );
    }
}


export default Home;