import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';

class Create extends React.Component{
    constructor(props) {
      super(props);
      this.onChangename           = this.onChangename.bind(this);
      this.onChangephone          = this.onChangephone.bind(this);
      this.onChangeaddress        = this.onChangeaddress.bind(this);
      this.onSubmit               = this.onSubmit.bind(this);
      this.state = {
          name: '',
          phone: '',
          address:''
      }
  }
  

  onChangename(e) {
    this.setState({
      name: e.target.value
    });
  }
  onChangephone(e) {
    this.setState({
      phone: e.target.value
    })  
  }
  onChangeaddress(e) {
    this.setState({
      address: e.target.value
    })
  }
  
  onSubmit(e) {
    e.preventDefault();
    const obj = {
      name: this.state.name,
      phone: this.state.phone,
      address: this.state.address
    };
      axios.post('http://localhost:3001/address_create/', obj)
        .then(res => console.log(res.data));
        this.props.history.push({
          pathname: '/home',
          state: { supplier_id: this.state.supplier_id}
        })
      this.setState({
      name: '',
      phone: '',
      address: ''
    })
  }
  
  pagedirect(){
  window.location.href = "/home";
  }
  
  home(){
  window.location.href = "/home";
  }
  
  
  render(){
      return(
    <div className="container">
    <div className="row">
          <div className='col-md-3'></div>
          <div className='col-md-6'>
          <div style={{marginTop: 10}}>
          <h3>Add New Address</h3>
              <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                      <label>Name  </label>
                      <input type="text" className="form-control" value={this.state.name}
                      onChange={this.onChangename} required/>
                  </div>
                  <div className="form-group">
                      <label>Phone</label>
                      <input type="text" className="form-control" value={this.state.phone}
                      onChange={this.onChangephone} required/>
                  </div>
                  <div className="form-group">
                      <label>Address</label>
                      <input type="text" className="form-control" value={this.state.address}
                      onChange={this.onChangeaddress} required/>
                  </div>
                  <div>
                </div>
                  <div className="form-group">
                      <input type="submit" value="Register" className="btn btn-primary"/>
                  </div>
              </form>
          </div>
          </div>
          <div className='col-md-3'></div>
        </div>
        </div>
  
      )
  }
  }


export default Create;