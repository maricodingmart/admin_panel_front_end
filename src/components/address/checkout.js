import React from 'react';
import axios from 'axios';
import Select from 'react-select';

const options2 = [
  { value: '1', label: '1' },
  { value: '2', label: '2' },
  { value: '3', label: '3' },
];

class Checkout extends React.Component{
    constructor(props) {
      super(props);
      this.onChangequantity          = this.onChangequantity.bind(this);
      this.onChangeaddress           = this.onChangeaddress.bind(this);
      this.onSubmit                  = this.onSubmit.bind(this);
      this.state = {
          quantity: '',
          address:'',
          selectedOption: '',
          options:[],
          product_id: this.props.match.params.id,
          user_id: localStorage.getItem('user_id')
      }
  }

  onChangequantity(e) {
    this.setState({
      quantity: e.target.value
    })  
  }
  onChangeaddress(e) {
    this.setState({
      address: e.target.value
    })
  }

  handleChange2 = quantity => {
    this.setState({ quantity });
    console.log(`Option selected:`, quantity);
  };


  componentDidMount(){
    console.log(this.props)
    let tempOptions = []
    axios.get("http://localhost:3001/address_all")
      .then(res => {
        res.data.map(i => tempOptions.push({"value":i.id, "label":i.name}))
        this.setState({ options: tempOptions })
      })
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };


  onSubmit(e) {
    e.preventDefault();
    const obj = {
      quantity: this.state.quantity.value,
      order_delivery_id: this.state.selectedOption.value,
      user_id: localStorage.getItem('user_id'),
      product_id: this.state.product_id
    };
      axios.post('http://localhost:3001/cart_create/', obj)
        .then(res => console.log(res.data));
          this.props.history.push({
            pathname: '/address/delivery',
            state: { user_id: this.state.user_id}
          })
          window.location.href = '/address/delivery'
      this.setState({
      quantity: '',
      address: ''
    })
  }
  
  
  render(){
    const { quantity } = this.state;
      return(
    <div className="container">
    <div className="row">
          <div className='col-md-3'></div>
          <div className='col-md-6'>
          <div style={{marginTop: 10}}>
          <h3>Enter your details to checkout your product</h3>
              <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                      <label>quantity</label>
                <Select value={this.state.quantity} onChange={this.handleChange2} options={options2} required></Select>
                  </div>
                  <div className="form-group">
                      <label>Address</label>
                    <Select value={this.state.selectedOption} onChange={this.handleChange}
                      options={this.state.options} required/>
                </div>
                  <div className="form-group">
                      <input type="submit" value="Register" className="btn btn-primary"/>
                  </div>
              </form>
          </div>
          </div>
          <div className='col-md-3'></div>
        </div>
        </div>
  
      )
  }
  }


export default Checkout;