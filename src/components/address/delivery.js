import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import {  NavLink } from "react-router-dom";

class Delivery extends React.Component{
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
        this.state = {
        product_list: [],
        cart_id: ''
        };
      }

    componentWillMount(){
        axios.get('http://localhost:3001/order_all?id='+localStorage.getItem('user_id'))
        .then(response => {
            console.log(response);
            this.setState({ product_list: response.data });
          })
          .catch(function (error) {
            console.log(error);
          })
    }

    delete=() => {
      axios.post('http://localhost:3001/cart_delete/?id='+this.state.product_list[0].id)
      this.props.history.push({
        pathname:"/address/delivery",
        state:{user_id: this.state.user_id}})
    }

    shipping=()=>{
      this.props.history.push({
        pathname:"/address/shipping/",
        state:{user_id: this.state.user_id,product_id: this.state.product_id}
      })
  }


    renderTableData() {
        return this.state.product_list.map((product_list, index) => {
          const { id,name,price,quantity,product,user,order_date} = product_list 
           return (
              <tr key={id}>
                  <td>{product.name}</td>
                 <td>{product.price}</td>
                 <td>{quantity}</td>
                 <td><button to={"/delete/"+id} className="btn btn-primary" onClick={this.delete}>Remove</button></td>
                 <td><NavLink to={"/address/shipping/"+id} onClick={this.shipping} className="btn btn-success">Checkout</NavLink></td>
                
                </tr>
           )
        })
     }

     page=()=>{
        this.props.history.push({
          pathname:"/customer_shop/",
          state:{user_id: this.state.user_id,product_id: this.state.product_id}
        })
    }


    render(){
        return(
            <div className="container">
                <div className="row">
                    <img src="https://www.loadette.com/mediaUpload//il_fullxfull.1419612206_gg1x-2-547x607.jpg" alt="shopping" style={{width:"300px",marginLeft:"420px"}}/>
                    <table className="table table-striped" style={{ marginTop: 20 }}>
                <thead>
              <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Delete</th>
                <th>Checkout</th>
                
              </tr>
            </thead>
            <tbody>
                  {this.renderTableData()}
            </tbody>

         </table>

                </div>
                <button className="btn btn-success" style={{float:"right"}} onClick={this.page}>Continue Shopping</button>
            </div>
        );
    }
}


export default Delivery;