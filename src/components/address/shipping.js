import React from 'react';
import axios from 'axios';


class Shipping extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        product_list: []
        };
      }
   
    componentDidMount(){
        axios.get('http://localhost:3001/shipping?id='+this.props.match.params.id)
        .then(response => {
            this.setState({ product_list: response.data });
          })
          .catch(function (error) {
            console.log(error);
          })
    }

    page=()=>{
      this.props.history.push({
        pathname:"/customer_shop/",
        state:{user_id: this.state.user_id}
      })
  }


    render(){
      return(
          <div>
            <span style={{fontSize:"100px",marginLeft:"630px"}}>&#10004;</span><br/>
            <h2 style={{textAlign:"center"}}> Your Order has been placed            <button className="btn btn-primary" onClick={this.page} style={{textAlign:"center",float:"left"}}>Continue shopping</button></h2>
            <table className="table">
              <tr>
              <th>Product Name</th>
              <td>{this.state.product_list && this.state.product_list.name ? this.state.product_list.name : null}</td>
              </tr>              <br/>
              <tr>
              <th>Price</th>
              <td> {this.state.product_list && this.state.product_list.price ? this.state.product_list.price : null}</td>
              </tr>              <br/>
              <tr>
              <th>Image</th>
              <td><img src={`http://localhost:3001/${this.state.product_list.image}`} style={{height:"140px",width:"500"}} alt="images"></img></td>
              </tr>
          </table>



          </div>
        );
    }
}


export default Shipping;