import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {  NavLink } from "react-router-dom";

class Customer extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        product_list: []
        };
      }

      componentDidMount(){
        axios.get('http://localhost:3001/customer_list')
        .then(response => {
            this.setState({ product_list: response.data });
          })
          .catch(function (error) {
            console.log(error);
          })
      }

      logout() {
        localStorage.clear();
        window.location.href = '/';
      }

    

        renderTableData() {
            return this.state.product_list.map((product_list, index) => {
              const { id,name,email,role,city} = product_list 
              
               return (
                  <tr key={id}>
                      <td>{name}</td>
                     <td>{email}</td>
                     <td>{role}</td>
                     <td>{city}</td>
                    </tr>
               )
            })
         }
         page(){
            window.location.href = '/admin'
         }

    render(){
        return(
            <div className="container">
                <h3 style={{textAlign:"center"}}>Customer List</h3><button className="btn btn-primary" onClick={this.page}>Seller List</button>
                <button className="btn btn-danger" style={{float:"right"}} onClick={this.logout}>Logout</button>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                <thead>
              <tr>
                <th>Name</th>
                <th>E-mail</th>
                <th>Role</th>
                <th>City</th>
                
              </tr>
            </thead>
            <tbody>
                  {this.renderTableData()}
            </tbody>
         </table>   
   </div>
        );
    }
}



export default Customer;