import React from 'react';
import axios from 'axios';
import Select from 'react-select';

const options = [
  { value: '2', label: 'Supplier' },
  { value: '3', label: 'Customer' },
];

class Register extends React.Component{
    constructor(props) {
      super(props);
      this.onChangename           = this.onChangename.bind(this);
      this.onChangeemail          = this.onChangeemail.bind(this);
      this.onChangepassword       = this.onChangepassword.bind(this);
      this.onSubmit               = this.onSubmit.bind(this);
      this.state = {
          name: '',
          email: '',
          password:'',
          selectedOption: null,
          userrole:''
      }
  }
  
  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };


  onChangename(e) {
    this.setState({
      name: e.target.value
    });
  }
  onChangeemail(e) {
    this.setState({
      email: e.target.value
    })  
  }
  onChangepassword(e) {
    this.setState({
      password: e.target.value
    })
  }

  
  onSubmit(e) {
    e.preventDefault();
    const obj = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      role: this.state.selectedOption.value
    };
      axios.post('http://localhost:3001/users/', obj)
        .then(res => console.log(res.data));
        this.props.history.push({
          pathname: '/'
        })
  }
  
  home(){
  window.location.href = "/";
  }
  
  
  render(){
    const { selectedOption } = this.state;
      return(
    <div className="container">
    <div className="row">
          <div className='col-md-3'></div>
          <div className='col-md-6'>
          <div style={{marginTop: 10}}>
          <h3>Register as a new user</h3>
              <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                      <label>Name  </label>
                      <input type="text" className="form-control" value={this.state.name}
                      onChange={this.onChangename} required/>
                  </div>
                  <div className="form-group">
                      <label>Email</label>
                      <input type="text" className="form-control" value={this.state.email}
                      onChange={this.onChangeemail} required/>
                  </div>
                  <div className="form-group">
                      <label>Password</label>
                      <input type="password" className="form-control" value={this.state.password}
                      onChange={this.onChangepassword} required/>
                  </div>
                  <div className="form-group">
                      <label>Select your role</label>
                      <Select value={this.state.selectedOption} onChange={this.handleChange} options={options} required/>
                </div>
                  <div className="form-group">
                      <input type="submit" value="Register" className="btn btn-primary"/>
                    <button className="btn btn-danger" style={{float:"right"}} onClick={this.home}>Back</button>
                  </div>
              </form>
          </div>
          </div>
          <div className='col-md-3'></div>
        </div>
        </div>
  
      )
  }
  }
  
  
  export default Register;