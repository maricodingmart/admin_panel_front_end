import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom'


class CustomerShop extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        product_list: [],
        user_id: '',
        product_id: ''
        };
      }

      componentDidMount(){
        axios.get('http://localhost:3001/all_products')
        .then(response => {
            this.setState({ product_list: response.data, product_id: this.props.match.params.id });
          })
          .catch(function (error) {
            console.log(error);
          })
      }

      logout() {
        localStorage.clear();
        window.location.href = '/';
      }

        renderTableData() {

            return this.state.product_list.map((product_list, index) => {
              const { id,name,price,stock,category,image} = product_list 
              
               return (
                  <tr key={id}>
                      <td>{name}</td>
                     <td>{price}</td>
                     <td>{stock}</td>
                     <td>{category.name}</td>
                     <td><img src={`http://localhost:3001/${image}`} style={{height:"140px",width:"500"}} alt="images"></img></td>
                     
                     <td><NavLink to={"/address/checkout/"+id} className="btn btn-success">Add to cart</NavLink></td>
                    </tr>
               )
            })
         }
         page=()=>{
             this.props.history.push({
               pathname:"/address/checkout/",
               state:{user_id: this.state.user_id,product_id: this.state.product_id}
             })
         }

         order=()=>{
          this.props.history.push({
            pathname:"/order",
            state:{user_id: this.state.user_id,product_id: this.state.product_id}
          })
      }




    render(){
        return(
            <div className="container">
                <h3 style={{textAlign:"center"}}> Welcome to Shopping </h3>
                <button className="btn btn-primary" style={{float:"left"}} onClick={this.order}>Your Orders</button>
               
                <button className="btn btn-danger" style={{float:"right"}} onClick={this.logout}>Logout</button>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                <thead>
              <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Stock</th>
                <th>Category Name</th>
                <th>Image</th>
                <th>Add to cart</th>
              </tr>
            </thead>
            <tbody>
                  {this.renderTableData()}
            </tbody>
         </table>   
   </div>
        );
    }
}


export default CustomerShop;