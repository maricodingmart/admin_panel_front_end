import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            user_id:''
        };   
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
    }
    
    handleChange(event){
        this.setState({[event.target.name]:event.target.value})
    }

    handleClick(){
        window.location.href="/register";
    }
 

    submit(e){
        e.preventDefault();
        axios.post('http://localhost:3001/auth/login',this.state)
          .then( (response) => {
            console.log(response);
            if (response.data.user_type === 1) {
                this.props.history.push({
                pathname: '/admin'
              })}       
            else if(response.data.user_type === 3){
                this.state.id = response.data.user_id
                this.props.history.push({
                      pathname: '/customer_shop/',
                      state: { user_id: this.state.id}
            })}
            else if(response.data.user_type === 2){
                this.state.id = response.data.user_id
                this.props.history.push({
                      pathname: '/home/',
                      state: { user_id: this.state.id}
            })}
            else{
            this.state.id = response.data.user_id
            this.props.history.push({
                pathname: '/',
                state: { user_id: this.state.id}
              })}
            localStorage.setItem('token', response.data.token)
            // localStorage.setItem('is_active',response.data.is_active)
            localStorage.setItem('user_type', response.data.user_type)
            localStorage.setItem('user_id',response.data.user_id)
          })
          .catch(function (error) {
            console.log(error);
          })
    }

    render() {
            return (
                <div className="container">
                    <div className="row">
                        <div className='col-md-3'></div>
                        <div className='col-md-6'>
                        <form onSubmit={this.submit}>
                    <div className="form-group">
                    <label>Username
                        <input type="text" className="form-control" name="email" onChange={this.handleChange} required/>
                   </label>
                   </div>
                   <div className="form-group">
                    <label>Password
                        <input type="password" className="form-control" name="password" onChange={this.handleChange} required/>
                    </label>
                    </div>
                    <button className="btn btn-primary" onClick={this.submit}>Login</button><br/><br/>              
                    <button className="btn btn-success" onClick={this.handleClick}>New User</button>
                    <br/>
                        </form>
                        </div>
                        <div className='col-md-3s'></div>
                    </div>
                </div>

 
            );
        }
    



}

export default Login;