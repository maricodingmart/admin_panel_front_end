import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Select from 'react-select';


class Create extends React.Component{
  constructor(props) {
    super(props);
    this.onChangename           = this.onChangename.bind(this);
    this.onChangeprice          = this.onChangeprice.bind(this);
    this.onChangestock    = this.onChangestock.bind(this);
    this.onChangesupplier    = this.onChangesupplier.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
        name: '',
        price: '',
        stock:'',
        image:'',
        supplier_id: this.props.location.state.user_id,
        selectedOption: '',
        options:[]
    }
}


handleFileChange = e => {
  this.setState({
    image: e.target.files[0],
  })
}

onChangesupplier(e) {
  this.setState({
    supplier_id: e.target.value
  });
}

onChangename(e) {
  this.setState({
    name: e.target.value
  });
}
onChangestock(e) {
  this.setState({
    stock: e.target.value
  })  
}
onChangeprice(e) {
  this.setState({
    price: e.target.value
  })
}

onSubmit(e) {
  e.preventDefault();
  const formData = new FormData();
  formData.append("image",this.state.image)
  formData.append( "name", this.state.name)
  formData.append("stock", this.state.stock)
  formData.append("price", this.state.price)
  formData.append("category_id", this.state.selectedOption.value)
  formData.append("supplier_id", this.state.supplier_id)
    axios.post('http://localhost:3001/product_create/', formData)
      .then(res => console.log(res.data));
      this.props.history.push({
        pathname: '/home',
        state: { supplier_id: this.state.supplier_id}
      })
}

componentDidMount(){
  let tempOptions = []
  axios.get("http://localhost:3001/category_all")
    .then(res => {
      res.data.map(i => tempOptions.push({"value":i.id, "label":i.name}))
      this.setState({ options: tempOptions })
    })
}
pagedirect(){
window.location.href = "/home";
}

handleChange = selectedOption => {
  this.setState({ selectedOption });
  console.log(`Option selected:`, selectedOption);
};

home(){
  this.props.history.push("/home");
}


render(){
    return(
  <div className="container">
  <div className="row">
        <div className='col-md-3'></div>
        <div className='col-md-6'>
        <div style={{marginTop: 10}}>
        <h3>Add New Product</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Name  </label>
                    <input type="text" className="form-control" value={this.state.name}
                    onChange={this.onChangename} required/>
                </div>
                <div className="form-group">
                    <label>Price</label>
                    <input type="text" className="form-control" value={this.state.price}
                    onChange={this.onChangeprice} required/>
                </div>
                <div className="form-group">
                    <label>Stock</label>
                    <input type="text" className="form-control" value={this.state.stock}
                    onChange={this.onChangestock} required/>
                </div>
                <div className="form-group">
                    <label>Category name</label>
                    <Select value={this.state.selectedOption} onChange={this.handleChange}
                      options={this.state.options} required/>
                </div>
                <div className="form-group">
                    <label>Product Image upload</label>
                <input name="image" type="file" onChange={this.handleFileChange} required></input>
                </div>
                <div className="form-group">
                    <input type="submit" value="Register" className="btn btn-primary"/>
                </div>
            </form>
        </div>
        </div>
        <div className='col-md-3'></div>
      </div>
      </div>

    )
}
}


export default Create;