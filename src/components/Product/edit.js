import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Select from 'react-select';

class Edit extends React.Component{
    constructor(props) {
        super(props);
        this.onChangename           = this.onChangename.bind(this);
        this.onChangeprice          = this.onChangeprice.bind(this);
        this.onChangestock    = this.onChangestock.bind(this);
        this.onChangeuser    = this.onChangeuser.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            name: '',
            price: '',
            stock:'',
            user_id: '',
            image:'',
            selectedOption: '',
            options:[]
        }
    }

    handleFileChange = e => {
      this.setState({
        image: e.target.files[0],
      })
    }

    onChangename(e) {
      this.setState({
        name: e.target.value
      });
    }

    handleChange = selectedOption => {
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
    };

    onChangestock(e) {
      this.setState({
        stock: e.target.value
      })  
    }
    onChangeprice(e) {
      this.setState({
        price: e.target.value
      })
    }

      onChangeuser(e) {
        this.setState({
          user_id: e.target.value
        })
      }

    componentWillMount(){
      let tempOptions = []
      axios.get("http://localhost:3001/category_all")
        .then(res => {
          console.log(res);
          res.data.map(i => tempOptions.push({"value":i.id, "label":i.name}))
          this.setState({ options: tempOptions })
        })
    }

    componentDidMount(){
      axios.get('http://localhost:3001/product_list?id='+this.props.match.params.id)
          .then(response => {
            console.log(response);
            this.setState({ 
                name: response.data.name,
                stock: response.data.stock,
                price: response.data.price,
                image: this.state.image,
                user_id: response.data.user.id,
                category_id : response.data.category.name
              });
          })
          .catch(function (error) {
              console.log(error);
          })
    }

   
    onSubmit(e) {
      e.preventDefault();
      const obj = {
        name: this.state.name,
        stock: this.state.stock,
        price: this.state.price,
        user_id: localStorage.getItem('user_id'),
        image: this.state.image,
        category_id: this.state.selectedOption.value
      };
      axios.post('http://localhost:3001/product_update?id='+this.props.match.params.id, obj)
      .then( (response) => {
        console.log(response);
        this.props.history.push({
            pathname: '/home',
            state: { user_id: this.state.user_id}
          })
      })
    }
   
    render(){
        return( 
          <div className="container">
            <div className="row">
              <div className="col-md-3"></div>
              <div className="col-md-6">
              <div style={{marginTop: 10}}>
                <h3>Edit User</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label> Name  </label>
                        <input type="text" className="form-control" value={this.state.name}
                        onChange={this.onChangename} />
                    </div>
                    <div className="form-group">
                        <label>Price </label>
                        <input type="text" className="form-control" value={this.state.price}
                        onChange={this.onChangeprice} />
                    </div>
                    <div className="form-group">
                        <label>Stock </label>
                        <input type="text" className="form-control" value={this.state.stock}
                        onChange={this.onChangestock} />
                    </div>
                    <div className="form-group">
                    <label>Category name</label>
                        <Select value={this.state.selectedOption} onChange={this.handleChange}
                      options={this.state.options}/>
                    </div>
                    <div className="form-group">
                    <label>Product Image upload</label>
                       <input name="image" type="file" onChange={this.handleFileChange}></input>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Update" onSubmit={this.onSubmit} className="btn btn-primary"/>
                    </div>
                    
                </form>
            </div>
              </div>
              <div className="col-md-3"></div>
           </div>
          </div>
        
        )
    }
}


export default Edit;