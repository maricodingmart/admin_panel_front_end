import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {  NavLink } from "react-router-dom";

class Admin extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        product_list: [],
        product_count: []
        };
      }

      componentDidMount(){
        axios.get('http://localhost:3001/seller_list')
        .then(response => {
            this.setState({ product_list: response.data });
          })
          .catch(function (error) {
            console.log(error);
          })
      }

      logout() {
        localStorage.clear();
        window.location.href = '/';
      }

        renderTableData() {
            return this.state.product_list.map((product_list, index) => {
                const { id,name,email,city,product_count} = product_list
             
                return (
                  <tr key={id}>
                      <td>{name}</td>
                     <td>{email}</td>
                     <td>{city}</td>
                     <td>{product_count}</td>
                    </tr>
               )
            })
         }
         
         customer(){
             window.location.href = '/customer';
         }

    render(){
        return(
            <div className="container">
                <h3 style={{textAlign:"center"}}> Seller List</h3><button className="btn btn-primary" onClick={this.customer}>Customer List</button>
                <button className="btn btn-danger" style={{float:"right"}} onClick={this.logout}>Logout</button>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                <thead>
              <tr>
                <th>Seller Name</th>
                <th>Seller E-mail</th>
                <th>City</th>
                <th>Product Count</th>
              </tr>
            </thead>
            <tbody>
                  {this.renderTableData()}
            </tbody>
         </table>   
   </div>
        );
    }
}


export default Admin;