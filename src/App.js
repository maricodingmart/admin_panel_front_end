import React from 'react';
import './App.css';
import Login from './components/login.js';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/home.js';
import Privateroute from './components/privateroute';
import Create from './components/Product/create';
import Edit from './components/Product/edit';
import Register from './components/User/register'
import Admin from './components/admin'
import Customer from './components/customer'
import CustomerShop from './components/User/customer_shop'
import Address from './components/address/create'
import AddressHome from './components/address/home'
import AddressCheckout from './components/address/checkout'
import AddressDelivery from './components/address/delivery'
import Shipping from './components/address/shipping'
import Order from './components/order'


const isLoggedIn = ()=>{
  const token =  localStorage.getItem('token');
  return token ? true : false;
}


function App() {
  return (
    <Router>
     <Route exact path="/" component={ Login } />
     <Privateroute path='/home/' component={ Home } auth={isLoggedIn()}/>
     <Privateroute path='/create/' component={ Create } auth={isLoggedIn()}/>
     <Route path='/register/' component={ Register }/>
     <Privateroute path='/edit/:id' component={ Edit } auth={isLoggedIn()}/>
     <Privateroute path='/admin/' component={ Admin } auth={isLoggedIn()}/>
     <Privateroute path='/customer/' component={ Customer } auth={isLoggedIn()}/>
     <Privateroute path='/customer_shop/' component={ CustomerShop } auth={isLoggedIn()}/>
     <Privateroute path='/address/create/:id' component={ Address } auth={isLoggedIn()}/>
     <Privateroute path='/address/home/' component={ AddressHome } auth={isLoggedIn()}/>
     <Privateroute path='/address/checkout/:id' component={ AddressCheckout } auth={isLoggedIn()}/>
     <Privateroute path='/address/delivery/' component={ AddressDelivery } auth={isLoggedIn()}/>
     <Privateroute path='/address/shipping/:id' component={ Shipping } auth={isLoggedIn()}/>
     <Privateroute path='/order/' component={ Order } auth={isLoggedIn()}/>
   </Router>
    );
}

export default App;


